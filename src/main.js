import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

// Это главный файл который запускает все приложение
// В new Vue мы создаем новый экземпляр Vue
// и с помощью метода render рендерим App компонент, который импортируем из файла /App.vue (2 строчка)
// после mount'им в #app (в папке public index <div id="app")