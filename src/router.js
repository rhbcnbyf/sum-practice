import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes:[
        {
            path: '/',
            component: Home
        },
        {
            path: '/active',
            component: () => import('./views/Active.vue')
        },
        {
            path: '/not-active',
            component: () => import('./views/NotActive.vue')
        }
    ]
})